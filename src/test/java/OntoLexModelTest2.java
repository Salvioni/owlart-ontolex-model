/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import it.uniroma2.art.ontolex.facilities.OWLArtModelFactoryWithOntoLexModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.vocabolary.OntoLex;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Andrea
 */
public class OntoLexModelTest2 {

    private static OntoLexModel model;
    private static final String directory = "C:\\Users\\Andrea\\Desktop\\test";
    private static final String baseUri = "http://www.example.com/";
    private static final String fileName = "cat.n3";

    @BeforeClass
    public static void setUpClass() {

        ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
        OWLArtModelFactoryWithOntoLexModel<Sesame2ModelConfiguration> fact = OWLArtModelFactoryWithOntoLexModel.createModelFactory(factImpl);
        try {
            model = fact.loadOntoLexModel(baseUri, directory);
        } catch (ModelCreationException ex) {
        }
    }

    @AfterClass
    public static void tearDownClass() {

        try {
            model.close();
        } catch (ModelUpdateException ex) {
        }

    }

    @Before
    public void setUp() {
        File f = new File(getClass().getClassLoader().getResource(fileName).getFile());
        try {
            model.addRDF(f, baseUri, RDFFormat.N3);
        } catch (IOException | ModelAccessException | ModelUpdateException | UnsupportedRDFFormatException ex) {
        }

    }

    @After
    public void tearDown() {

        try {
            model.clearRDF();
        } catch (ModelUpdateException ex) {
        }

    }
    
    
       @Test
    public void listFormsByWrittenRepTest() {

        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource(baseUri + "cat_form"));

        try {

            try (ARTURIResourceIterator it = model.listFormsByWrittenRep("CAT","en")) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException ex) {
            fail();
        }

    }
    

    @Test
    public void isLexicalSenseTest() {

        ARTURIResource lexicalSense = model.createURIResource(baseUri + "cat_sense");

        try {
            assertTrue(model.isLexicalSense(lexicalSense));
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void getReferenceTest() {

        ARTURIResource lexicalSense = model.createURIResource(baseUri + "cat_sense");
        ARTURIResource expecteds = model.createURIResource("http://dbpedia.org/resource/Cat");

        try {
            ARTURIResource actuals = model.getReference(lexicalSense);
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void setCanonicalFormTest() {

        ARTURIResource lexicalSense = model.createURIResource(baseUri + "cat_sense");
        ARTURIResource oldReference = model.createURIResource("http://dbpedia.org/resource/Cat");
        ARTURIResource expecteds = model.createURIResource("http://dbpedia.org/resource/Cat1");

        try {

            model.setReference(lexicalSense, expecteds);

            ARTURIResource actuals = model.getReference(lexicalSense);

            assertTrue(!(model.hasTriple(lexicalSense, OntoLex.Res.CANONICAL_FORM, oldReference, true)) && expecteds.equals(actuals));

        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void listDenotesTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource("http://dbpedia.org/resource/Cat"));

        try {

            try (ARTURIResourceIterator it = model.listDenotes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void addDenotesTest() {
        ARTURIResource newOntologyEntity = model.createURIResource("http://dbpedia.org/resource/Cat1");
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource("http://dbpedia.org/resource/Cat"));
        expecteds.add(newOntologyEntity);

        try {

            model.addDenotes(lexicalEntry, newOntologyEntity);

            try (ARTURIResourceIterator it = model.listDenotes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void removeDenotesTest() {
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();

        try {

            model.removeDenotes(lexicalEntry, model.createURIResource("http://dbpedia.org/resource/Cat"));

            try (ARTURIResourceIterator it = model.listDenotes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    
       @Test
    public void listSensesTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource(baseUri+"cat_sense"));

        try {

            try (ARTURIResourceIterator it = model.listSenses(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void addSenseTest() {
        ARTURIResource newSense = model.createURIResource(baseUri + "cat_sense2");
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource(baseUri + "cat_sense"));
        expecteds.add(newSense);

        try {

            model.addSense(lexicalEntry, newSense);

            try (ARTURIResourceIterator it = model.listSenses(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void removeSenseTest() {
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();

        try {

            model.removeSense(lexicalEntry, model.createURIResource(baseUri+"cat_sense"));

            try (ARTURIResourceIterator it = model.listSenses(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }
    
    
           @Test
    public void listEvokesTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource("http://wordnet-rdf.princeton.edu/wn31/102124272-n"));

        try {

            try (ARTURIResourceIterator it = model.listEvokes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void addEvokesTest() {
        ARTURIResource newLexicalConcept = model.createURIResource("http://wordnet-rdf.princeton.edu/wn31/999999999-n");
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();
        expecteds.add(model.createURIResource("http://wordnet-rdf.princeton.edu/wn31/102124272-n"));
        expecteds.add(newLexicalConcept);

        try {

            model.addEvokes(lexicalEntry, newLexicalConcept);

            try (ARTURIResourceIterator it = model.listEvokes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void removeEvokesTest() {
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "cat_lex");
        HashSet<ARTURIResource> expecteds = new HashSet<ARTURIResource>();
        HashSet<ARTURIResource> actuals = new HashSet<ARTURIResource>();

        try {

            model.removeEvokes(lexicalEntry, model.createURIResource("http://wordnet-rdf.princeton.edu/wn31/102124272-n"));

            try (ARTURIResourceIterator it = model.listEvokes(lexicalEntry)) {
                while (it.hasNext()) {
                    actuals.add(it.next());
                }
            }
            assertEquals(expecteds, actuals);
        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }
    
    
}
