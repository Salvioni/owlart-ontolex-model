
import it.uniroma2.art.ontolex.facilities.OWLArtModelFactoryWithOntoLexModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.vocabolary.OntoLex;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.UnsupportedRDFFormatException;
import it.uniroma2.art.owlart.io.RDFFormat;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;
import it.uniroma2.art.owlart.sesame2impl.models.conf.Sesame2ModelConfiguration;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class OntoLexModelTest1 {

    private static OntoLexModel model;
    private static final String directory = "C:\\Users\\Andrea\\Desktop\\test";
    private static final String baseUri = "http://www.example.com/";
    private static final String fileName = "color.n3";

    @BeforeClass
    public static void setUpClass() {

        ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();
        OWLArtModelFactoryWithOntoLexModel<Sesame2ModelConfiguration> fact = OWLArtModelFactoryWithOntoLexModel.createModelFactory(factImpl);
        try {
            model = fact.loadOntoLexModel(baseUri, directory);
        } catch (ModelCreationException ex) {
        }
    }

    @AfterClass
    public static void tearDownClass() {

        try {
            model.close();
        } catch (ModelUpdateException ex) {
        }

    }

    @Before
    public void setUp() {
        File f = new File(getClass().getClassLoader().getResource(fileName).getFile());
        try {
            model.addRDF(f, baseUri, RDFFormat.N3);
        } catch (IOException | ModelAccessException | ModelUpdateException | UnsupportedRDFFormatException ex) {
        }

    }

    @After
    public void tearDown() {

        try {
            model.clearRDF();
        } catch (ModelUpdateException ex) {
        }

    }

    @Test
    public void isLexicalEntryTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "lex_color");

        try {
            assertTrue(model.isLexicalEntry(lexicalEntry));
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void isFormTest() {

        ARTURIResource form = model.createURIResource(baseUri + "form_color");

        try {
            assertTrue(model.isForm(form));
        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void listWrittenRepsTest() {

        HashSet<ARTLiteral> expecteds = new HashSet<ARTLiteral>();
        expecteds.add(model.createLiteral("colour", "en-gb"));
        expecteds.add(model.createLiteral("color", "en-us"));

        ARTURIResource form = model.createURIResource(baseUri + "form_color");

        try {

            HashSet<ARTLiteral> actuals = new HashSet<ARTLiteral>();

            try (ARTLiteralIterator it = model.listWrittenReps(form)) {

                while (it.hasNext()) {
                    actuals.add(it.next());
                }

            }

            assertEquals(expecteds, actuals);

        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void listWrittenRepsWithLanguageTest() {

        HashSet<ARTLiteral> expecteds = new HashSet<ARTLiteral>();
        expecteds.add(model.createLiteral("colour", "en-gb"));

        ARTURIResource form = model.createURIResource(baseUri + "form_color");

        try {

            HashSet<ARTLiteral> actuals = new HashSet<ARTLiteral>();

            try (ARTLiteralIterator it = model.listWrittenReps(form, "en-gb")) {

                while (it.hasNext()) {
                    actuals.add(it.next());
                }

            }

            assertEquals(expecteds, actuals);

        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void getCanonicalFormTest() {

        ARTURIResource expecteds = model.createURIResource(baseUri + "form_color");
        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "lex_color");

        try {

            ARTURIResource actuals = model.getCanonicalForm(lexicalEntry);

            assertEquals(expecteds, actuals);

        } catch (ModelAccessException ex) {
            fail();
        }

    }

    @Test
    public void removeCanonicalFormTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "lex_color");

        try {

            model.removeCanonicalForm(lexicalEntry);

            ARTURIResource form = model.getCanonicalForm(lexicalEntry);

            assertNull(form);

        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void setCanonicalFormTest() {

        ARTURIResource lexicalEntry = model.createURIResource(baseUri + "lex_color");
        ARTURIResource oldForm = model.createURIResource(baseUri + "form_color");
        ARTURIResource expecteds = model.createURIResource(baseUri + "new_form_color");

        try {

            model.setCanonicalForm(lexicalEntry, expecteds);

            ARTURIResource actuals = model.getCanonicalForm(lexicalEntry);

            assertTrue(!(model.hasTriple(lexicalEntry, OntoLex.Res.CANONICAL_FORM, oldForm, true)) && expecteds.equals(actuals));

        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void addWrittenRepTest() {

        ARTLiteral literal = model.createLiteral("colore", "it");
        ARTURIResource form = model.createURIResource(baseUri + "form_color");

        try {
            model.addWrittenRep(form, literal);
            assertTrue(model.hasTriple(form, OntoLex.Res.WRITTEN_REP, literal, false));

        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

    @Test
    public void removeWrittenRepTest() {

        ARTLiteral literal = model.createLiteral("colour", "en-gb");
        ARTURIResource form = model.createURIResource(baseUri + "form_color");

        try {
            model.removeWrittenRep(form, literal);
            assertFalse(model.hasTriple(form, OntoLex.Res.WRITTEN_REP, literal, false));

        } catch (ModelAccessException | ModelUpdateException ex) {
            fail();
        }

    }

}
