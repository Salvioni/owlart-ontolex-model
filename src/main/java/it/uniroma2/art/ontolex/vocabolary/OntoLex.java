package it.uniroma2.art.ontolex.vocabolary;

import it.uniroma2.art.owlart.exceptions.VocabularyInitializationException;
import it.uniroma2.art.owlart.model.ARTNodeFactory;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.vocabulary.VocabUtilities;

public class OntoLex {

    public static final String URI = "http://www.w3.org/ns/lemon/ontolex";
    public static final String NAMESPACE = "http://www.w3.org/ns/lemon/ontolex#";

    public static final String LEXICAL_ENTRY = "http://www.w3.org/ns/lemon/ontolex#LexicalEntry";
    public static final String WORD = "http://www.w3.org/ns/lemon/ontolex#Word";
    public static final String MULTIWORD_EXPRESSION = "http://www.w3.org/ns/lemon/ontolex#MultiwordExpression";
    public static final String AFFIX = "http://www.w3.org/ns/lemon/ontolex#Affix";
    public static final String FORM = "http://www.w3.org/ns/lemon/ontolex#Form";
    public static final String LEXICAL_SENSE = "http://www.w3.org/ns/lemon/ontolex#LexicalSense";
    public static final String LEXICAL_CONCEPT = "http://www.w3.org/ns/lemon/ontolex#LexicalConcept";
    public static final String CONCEPT_SET = "http://www.w3.org/ns/lemon/ontolex#ConceptSet";

    public final static String WRITTEN_REP = "http://www.w3.org/ns/lemon/ontolex#writtenRep";
    public final static String PHONETIC_REP = "http://www.w3.org/ns/lemon/ontolex#phoneticRep";
    public final static String REPRESENTATION = "http://www.w3.org/ns/lemon/ontolex#representation";

    public final static String CANONICAL_FORM = "http://www.w3.org/ns/lemon/ontolex#canonicalForm";
    public final static String OTHER_FORM = "http://www.w3.org/ns/lemon/ontolex#otherForm";
    public final static String LEXICAL_FORM = "http://www.w3.org/ns/lemon/ontolex#lexicalForm";

    public final static String DENOTES = "http://www.w3.org/ns/lemon/ontolex#denotes";
    public final static String IS_DENOTED_BY = "http://www.w3.org/ns/lemon/ontolex#isDenotedBy";

    public final static String SENSE = "http://www.w3.org/ns/lemon/ontolex#sense";
    public final static String IS_SENSE_OF = "http://www.w3.org/ns/lemon/ontolex#isSenseOf";

    public final static String REFERENCE = "http://www.w3.org/ns/lemon/ontolex#reference";
    public final static String IS_REFERENCE_OF = "http://www.w3.org/ns/lemon/ontolex#isReferenceOf";

    public final static String MORPHOLOGICAL_PATTERN = "http://www.w3.org/ns/lemon/ontolex#morphologicalPattern";

    public final static String USAGE = "http://www.w3.org/ns/lemon/ontolex#usage";

    public final static String EVOKES = "http://www.w3.org/ns/lemon/ontolex#evokes";
    public final static String IS_EVOKED_BY = "http://www.w3.org/ns/lemon/ontolex#isEvokedBy";

    public final static String LEXICALIZED_SENSE = "http://www.w3.org/ns/lemon/ontolex#lexicalizedSense";
    public final static String IS_LEXICALIZED_SENSE_OF = "http://www.w3.org/ns/lemon/ontolex#isLexicalizedSenseOf";

    public final static String CONCEPT = "http://www.w3.org/ns/lemon/ontolex#concept";
    public final static String IS_CONCEPT_OF = "http://www.w3.org/ns/lemon/ontolex#isConceptOf";

    public static class Res {

        public static ARTURIResource URI;
        public static ARTURIResource NAMESPACE;

        public static ARTURIResource LEXICAL_ENTRY;
        public static ARTURIResource WORD;
        public static ARTURIResource MULTIWORD_EXPRESSION;
        public static ARTURIResource AFFIX;
        public static ARTURIResource FORM;
        public static ARTURIResource LEXICAL_SENSE;
        public static ARTURIResource LEXICAL_CONCEPT;
        public static ARTURIResource CONCEPT_SET;

        public static ARTURIResource WRITTEN_REP;
        public static ARTURIResource PHONETIC_REP;
        public static ARTURIResource REPRESENTATION;

        public static ARTURIResource CANONICAL_FORM;
        public static ARTURIResource OTHER_FORM;
        public static ARTURIResource LEXICAL_FORM;

        public static ARTURIResource DENOTES;
        public static ARTURIResource IS_DENOTED_BY;

        public static ARTURIResource SENSE;
        public static ARTURIResource IS_SENSE_OF;

        public static ARTURIResource REFERENCE;
        public static ARTURIResource IS_REFERENCE_OF;

        public static ARTURIResource MORPHOLOGICAL_PATTERN;

        public static ARTURIResource USAGE;

        public static ARTURIResource EVOKES;
        public static ARTURIResource IS_EVOKED_BY;

        public static ARTURIResource LEXICALIZED_SENSE;
        public static ARTURIResource IS_LEXICALIZED_SENSE_OF;

        public static ARTURIResource CONCEPT;
        public static ARTURIResource IS_CONCEPT_OF;

        static {
            try {
                initialize(VocabUtilities.nodeFactory);
            } catch (VocabularyInitializationException e) {
                e.printStackTrace(System.err);
            }
        }

        public static void initialize(ARTNodeFactory fact) throws VocabularyInitializationException {

            URI = fact.createURIResource(OntoLex.URI);
            NAMESPACE = fact.createURIResource(OntoLex.NAMESPACE);

            LEXICAL_ENTRY = fact.createURIResource(OntoLex.LEXICAL_ENTRY);
            WORD = fact.createURIResource(OntoLex.WORD);
            MULTIWORD_EXPRESSION = fact.createURIResource(OntoLex.MULTIWORD_EXPRESSION);
            AFFIX = fact.createURIResource(OntoLex.AFFIX);
            FORM = fact.createURIResource(OntoLex.FORM);
            LEXICAL_SENSE = fact.createURIResource(OntoLex.LEXICAL_SENSE);
            LEXICAL_CONCEPT = fact.createURIResource(OntoLex.LEXICAL_CONCEPT);
            CONCEPT_SET = fact.createURIResource(OntoLex.CONCEPT_SET);

            WRITTEN_REP = fact.createURIResource(OntoLex.WRITTEN_REP);
            PHONETIC_REP = fact.createURIResource(OntoLex.PHONETIC_REP);
            REPRESENTATION = fact.createURIResource(OntoLex.REPRESENTATION);

            CANONICAL_FORM = fact.createURIResource(OntoLex.CANONICAL_FORM);
            OTHER_FORM = fact.createURIResource(OntoLex.OTHER_FORM);
            LEXICAL_FORM = fact.createURIResource(OntoLex.LEXICAL_FORM);

            DENOTES = fact.createURIResource(OntoLex.DENOTES);
            IS_DENOTED_BY = fact.createURIResource(OntoLex.IS_DENOTED_BY);

            SENSE = fact.createURIResource(OntoLex.SENSE);
            IS_SENSE_OF = fact.createURIResource(OntoLex.IS_SENSE_OF);

            REFERENCE = fact.createURIResource(OntoLex.REFERENCE);
            IS_REFERENCE_OF = fact.createURIResource(OntoLex.IS_REFERENCE_OF);

            MORPHOLOGICAL_PATTERN = fact.createURIResource(OntoLex.MORPHOLOGICAL_PATTERN);

            USAGE = fact.createURIResource(OntoLex.USAGE);

            EVOKES = fact.createURIResource(OntoLex.EVOKES);
            IS_EVOKED_BY = fact.createURIResource(OntoLex.IS_EVOKED_BY);

            LEXICALIZED_SENSE = fact.createURIResource(OntoLex.LEXICALIZED_SENSE);
            IS_LEXICALIZED_SENSE_OF = fact.createURIResource(OntoLex.IS_LEXICALIZED_SENSE_OF);

            CONCEPT = fact.createURIResource(OntoLex.CONCEPT);
            IS_CONCEPT_OF = fact.createURIResource(OntoLex.IS_CONCEPT_OF);

        }

    }

}
