/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.uniroma2.art.ontolex.facilities;

import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.model.impl.OntoLexModelImpl;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.LinkedDataResolver;
import it.uniroma2.art.owlart.models.ModelFactory;
import it.uniroma2.art.owlart.models.OWLArtModelFactory;
import it.uniroma2.art.owlart.models.OWLModel;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.models.RDFSModel;
import it.uniroma2.art.owlart.models.SKOSModel;
import it.uniroma2.art.owlart.models.SKOSXLModel;
import it.uniroma2.art.owlart.models.TripleQueryModelHTTPConnection;
import it.uniroma2.art.owlart.models.UnloadableModelConfigurationException;
import it.uniroma2.art.owlart.models.UnsupportedModelConfigurationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.models.impl.SPARQLBasedRDFTripleModelImpl;
import java.util.Collection;

/**
 *
 * @author Andrea
 */
public class OWLArtModelFactoryWithOntoLexModel<MC extends ModelConfiguration> implements ModelFactory<MC> {

    private OWLArtModelFactory<MC> modelFactoryImpl;

    private OWLArtModelFactoryWithOntoLexModel(ModelFactory<MC> mf) {
        this.modelFactoryImpl = OWLArtModelFactory.createModelFactory(mf);
    }

    public static <MC extends ModelConfiguration> OWLArtModelFactoryWithOntoLexModel<MC> createModelFactory(ModelFactory<MC> mf) {
        return new OWLArtModelFactoryWithOntoLexModel<MC>(mf);
    }

    public <MCImpl extends MC> OntoLexModel loadOntoLexModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return new OntoLexModelImpl(modelFactoryImpl.loadRDFBaseModel(string, string1, mcimpl));
    }

    public <MCImpl extends MC> OntoLexModel loadOntoLexModel(String string, String string1) throws ModelCreationException {
        return new OntoLexModelImpl(modelFactoryImpl.loadRDFBaseModel(string, string1));
    }

    public OntoLexModel loadOntoLexModel(String string) throws ModelCreationException {
        TripleQueryModelHTTPConnection conn = modelFactoryImpl.loadTripleQueryHTTPConnection(string);
        return new OntoLexModelImpl(new SPARQLBasedRDFTripleModelImpl(conn));
    }

    @Override
    public <MCImpl extends MC> MCImpl createModelConfigurationObject(Class<MCImpl> type) throws UnsupportedModelConfigurationException, UnloadableModelConfigurationException {
        return modelFactoryImpl.createModelConfigurationObject(type);
    }

    @Override
    public Collection<Class<? extends MC>> getModelConfigurations() {
        return modelFactoryImpl.getModelConfigurations();
    }

    @Override
    public <MCImpl extends MC> BaseRDFTripleModel loadRDFBaseModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadRDFBaseModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> BaseRDFTripleModel loadRDFBaseModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadRDFBaseModel(string, string1);
    }

    @Override
    public <MCImpl extends MC> RDFModel loadRDFModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadRDFModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> RDFModel loadRDFModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadRDFModel(string, string1);
    }

    @Override
    public <MCImpl extends MC> RDFSModel loadRDFSModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadRDFSModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> RDFSModel loadRDFSModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadRDFSModel(string, string1);
    }

    @Override
    public <MCImpl extends MC> OWLModel loadOWLModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadOWLModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> OWLModel loadOWLModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadOWLModel(string, string1);
    }

    @Override
    public <MCImpl extends MC> SKOSModel loadSKOSModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadSKOSModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> SKOSModel loadSKOSModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadSKOSModel(string, string1);
    }

    @Override
    public <MCImpl extends MC> SKOSXLModel loadSKOSXLModel(String string, String string1, MCImpl mcimpl) throws ModelCreationException {
        return modelFactoryImpl.loadSKOSXLModel(string, string1, mcimpl);
    }

    public <MCImpl extends MC> SKOSXLModel loadSKOSXLModel(String string, String string1) throws ModelCreationException {
        return modelFactoryImpl.loadSKOSXLModel(string, string1);
    }

    @Override
    public TripleQueryModelHTTPConnection loadTripleQueryHTTPConnection(String string) throws ModelCreationException {
        return modelFactoryImpl.loadTripleQueryHTTPConnection(string);
    }

    @Override
    public LinkedDataResolver loadLinkedDataResolver() {
        return modelFactoryImpl.loadLinkedDataResolver();
    }

    @Override
    public void closeModel(BaseRDFTripleModel brdftm) throws ModelUpdateException {
        modelFactoryImpl.closeModel(brdftm);
    }

    @Override
    public void setPopulatingW3CVocabularies(boolean bln) {
        modelFactoryImpl.setPopulatingW3CVocabularies(bln);
    }

    @Override
    public boolean isPopulatingW3CVocabularies() {
        return modelFactoryImpl.isPopulatingW3CVocabularies();
    }

    @Override
    public BaseRDFTripleModel createLightweightRDFModel() {
        return modelFactoryImpl.createLightweightRDFModel();
    }

}
