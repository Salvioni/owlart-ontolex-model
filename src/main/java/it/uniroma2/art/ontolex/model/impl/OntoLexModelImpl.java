package it.uniroma2.art.ontolex.model.impl;

import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.ontolex.vocabolary.OntoLex;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.exceptions.QueryEvaluationException;
import it.uniroma2.art.owlart.exceptions.UnsupportedQueryLanguageException;
import it.uniroma2.art.owlart.io.RDFNodeSerializer;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTNode;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.model.NodeFilters;
import it.uniroma2.art.owlart.models.BaseRDFTripleModel;
import it.uniroma2.art.owlart.models.impl.LiteralIteratorFilteringLanguage;
import it.uniroma2.art.owlart.models.impl.LiteralIteratorWrappingNodeIterator;
import it.uniroma2.art.owlart.models.impl.RDFModelImpl;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorFilteringNodeIterator;
import it.uniroma2.art.owlart.models.impl.URIResourceIteratorFilteringResourceIterator;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTNodeIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.query.MalformedQueryException;
import it.uniroma2.art.owlart.query.QueryLanguage;
import it.uniroma2.art.owlart.query.TupleBindings;
import it.uniroma2.art.owlart.query.TupleBindingsIterator;
import it.uniroma2.art.owlart.query.TupleQuery;
import it.uniroma2.art.owlart.utilities.RDFIterators;
import it.uniroma2.art.owlart.vocabulary.RDF;
import java.util.ArrayList;
import java.util.Collection;

public class OntoLexModelImpl extends RDFModelImpl implements OntoLexModel {

    public OntoLexModelImpl(BaseRDFTripleModel baseRep) {
        super(baseRep);
    }

    @Override
    public boolean isAffix(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.AFFIX, true, graphs));
    }

    @Override
    public boolean isConceptSet(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.CONCEPT_SET, true, graphs));
    }

    @Override
    public boolean isForm(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.FORM, true, graphs));
    }

    @Override
    public boolean isLexicalConcept(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.LEXICAL_CONCEPT, true, graphs));
    }

    @Override
    public boolean isLexicalEntry(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.LEXICAL_ENTRY, true, graphs));
    }

    @Override
    public boolean isLexicalSense(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.LEXICAL_SENSE, true, graphs));
    }

    @Override
    public boolean isMultiwordExpression(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.MULTIWORD_EXPRESSION, true, graphs));
    }

    @Override
    public boolean isWord(ARTResource resource, ARTResource... graphs) throws ModelAccessException {
        return (baseRep.hasTriple(resource, RDF.Res.TYPE, OntoLex.Res.WORD, true, graphs));
    }

    @Override
    public ARTURIResourceIterator listAffixes(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.AFFIX, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listConceptSet(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.CONCEPT_SET, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listForms(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.FORM, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listFormsByWrittenRep(String term, String language, ARTResource... graphs) throws ModelAccessException {

        ARTLiteral literal = baseRep.createLiteral(term, language);
        return listFormsByWrittenRep(literal, graphs);

    }

    @Override
    public ARTURIResourceIterator listFormsByWrittenRep(ARTLiteral literal, ARTResource... graphs) throws ModelAccessException {

        Collection<ARTURIResource> result = new ArrayList<ARTURIResource>();

        String queryString = "select distinct ?form where { "
                + "?form " + RDFNodeSerializer.toNT(OntoLex.Res.WRITTEN_REP) + " ?rep. "
                + "FILTER (lcase(str(?rep)) = \"" + literal.getLabel().toLowerCase() + "\"). "
                + "FILTER(lang(?rep)=\"" + literal.getLanguage() + "\"). "
                + "}";

        try {
            TupleQuery query = baseRep.createTupleQuery(QueryLanguage.SPARQL, queryString, baseRep.getBaseURI());

            try (TupleBindingsIterator it = query.evaluate(true)) {

                while (it.hasNext()) {

                    TupleBindings next = it.next();

                    if (next.getBoundValue("form").isURIResource()) {

                        result.add(next.getBoundValue("form").asURIResource());

                    }

                }

            }

        } catch (UnsupportedQueryLanguageException | MalformedQueryException | QueryEvaluationException ex) {

            throw new ModelAccessException(ex);

        }

        return RDFIterators.createARTURIResourceIterator(result.iterator());

    }

    @Override
    public ARTURIResourceIterator listLexicalConcepts(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.LEXICAL_CONCEPT, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listLexicalEntries(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.LEXICAL_ENTRY, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listLexicalEntriesByLexicalForm(ARTURIResource lexicalForm, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(OntoLex.Res.LEXICAL_FORM, lexicalForm, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listLexicalSenses(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.LEXICAL_SENSE, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listMultiwordExpressions(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.MULTIWORD_EXPRESSION, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listWords(ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringResourceIterator(baseRep.listSubjectsOfPredObjPair(RDF.Res.TYPE, OntoLex.Res.WORD, true, graphs));

    }

    @Override
    public ARTURIResourceIterator listOtherForms(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.OTHER_FORM, false, graphs));

    }

    @Override
    public void addOtherForm(ARTURIResource lexicalEntry, ARTURIResource form, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, OntoLex.Res.OTHER_FORM, form, graphs);

    }

    @Override
    public void removeOtherForm(ARTURIResource lexicalEntry, ARTURIResource form, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.OTHER_FORM, form, graphs);

    }

    @Override
    public ARTURIResource getCanonicalForm(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.CANONICAL_FORM, false, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;
    }

    @Override
    public void setCanonicalForm(ARTURIResource lexicalEntry, ARTURIResource form, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.CANONICAL_FORM, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicalEntry, OntoLex.Res.CANONICAL_FORM, form, graphs);

    }

    @Override
    public void removeCanonicalForm(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.CANONICAL_FORM, NodeFilters.ANY, graphs);

    }

    @Override
    public ARTURIResource getLanguage(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicalEntry, baseRep.createURIResource("http://purl.org/dc/terms/language"), false, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setLanguage(ARTURIResource lexicalEntry, ARTURIResource language, ARTResource... graphs) throws ModelUpdateException {

        ARTURIResource languageProperty = baseRep.createURIResource("http://purl.org/dc/terms/language");

        baseRep.deleteTriple(lexicalEntry, languageProperty, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicalEntry, languageProperty, language, graphs);

    }

    @Override
    public ARTURIResourceIterator listLexicalForms(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.LEXICAL_FORM, true, graphs));

    }

    @Override
    public void addLexicalForm(ARTURIResource lexicalEntry, ARTURIResource form, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, OntoLex.Res.LEXICAL_FORM, form, graphs);

    }

    @Override
    public void removeLexicalForm(ARTURIResource lexicalEntry, ARTURIResource form, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.LEXICAL_FORM, NodeFilters.ANY, graphs);

    }

    @Override
    public ARTURIResourceIterator listDenotes(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.DENOTES, true, graphs));

    }

    @Override
    public void addDenotes(ARTURIResource lexicalEntry, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, OntoLex.Res.DENOTES, ontologyEntity, graphs);
    }

    @Override
    public void removeDenotes(ARTURIResource lexicalEntry, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.DENOTES, ontologyEntity, graphs);
    }

    @Override
    public ARTURIResourceIterator listSenses(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.SENSE, true, graphs));

    }

    @Override
    public void addSense(ARTURIResource lexicalEntry, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, OntoLex.Res.SENSE, lexicalSense, graphs);

    }

    @Override
    public void removeSense(ARTURIResource lexicalEntry, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.SENSE, lexicalSense, graphs);

    }

    @Override
    public ARTURIResourceIterator listEvokes(ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalEntry, OntoLex.Res.EVOKES, true, graphs));

    }

    @Override
    public void addEvokes(ARTURIResource lexicalEntry, ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalEntry, OntoLex.Res.EVOKES, lexicalConcept, graphs);

    }

    @Override
    public void removeEvokes(ARTURIResource lexicalEntry, ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalEntry, OntoLex.Res.EVOKES, lexicalConcept, graphs);

    }

    @Override
    public ARTLiteralIterator listWrittenReps(ARTURIResource form, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorWrappingNodeIterator(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.WRITTEN_REP, false, graphs));

    }

    @Override
    public ARTLiteralIterator listWrittenReps(ARTURIResource form, String language, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorFilteringLanguage(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.WRITTEN_REP, false, graphs), language);

    }

    @Override
    public void addWrittenRep(ARTURIResource form, ARTLiteral writtenRep, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(form, OntoLex.Res.WRITTEN_REP, writtenRep, graphs);

    }

    @Override
    public void addWrittenRep(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        addWrittenRep(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public void removeWrittenRep(ARTURIResource form, ARTLiteral writtenRep, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(form, OntoLex.Res.WRITTEN_REP, writtenRep, graphs);

    }

    @Override
    public void removeWrittenRep(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        removeWrittenRep(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public ARTLiteralIterator listPhoneticReps(ARTURIResource form, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorWrappingNodeIterator(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.PHONETIC_REP, false, graphs));

    }

    @Override
    public ARTLiteralIterator listPhoneticReps(ARTURIResource form, String language, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorFilteringLanguage(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.PHONETIC_REP, false, graphs), language);

    }

    @Override
    public void addPhoneticRep(ARTURIResource form, ARTLiteral phoneticRep, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(form, OntoLex.Res.PHONETIC_REP, phoneticRep, graphs);

    }

    @Override
    public void addPhoneticRep(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        addPhoneticRep(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public void removePhoneticRep(ARTURIResource form, ARTLiteral phoneticRep, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(form, OntoLex.Res.PHONETIC_REP, phoneticRep, graphs);

    }

    @Override
    public void removePhoneticRep(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        removePhoneticRep(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public ARTLiteralIterator listRepresentations(ARTURIResource form, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorWrappingNodeIterator(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.REPRESENTATION, true, graphs));

    }

    @Override
    public ARTLiteralIterator listRepresentations(ARTURIResource form, String language, ARTResource... graphs) throws ModelAccessException {

        return new LiteralIteratorFilteringLanguage(baseRep.listValuesOfSubjPredPair(form, OntoLex.Res.REPRESENTATION, true, graphs), language);

    }

    @Override
    public void addRepresentation(ARTURIResource form, ARTLiteral representation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(form, OntoLex.Res.REPRESENTATION, representation, graphs);

    }

    @Override
    public void addRepresentation(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        addRepresentation(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public void removeRepresentation(ARTURIResource form, ARTLiteral representation, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(form, OntoLex.Res.REPRESENTATION, representation, graphs);

    }

    @Override
    public void removeRepresentation(ARTURIResource form, String term, String language, ARTResource... graphs) throws ModelUpdateException {

        removeRepresentation(form, baseRep.createLiteral(term, language), graphs);

    }

    @Override
    public ARTURIResourceIterator listIsEvokedBy(ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalConcept, OntoLex.Res.IS_EVOKED_BY, true, graphs));

    }

    @Override
    public void addIsEvokedBy(ARTURIResource lexicalConcept, ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalConcept, OntoLex.Res.IS_EVOKED_BY, lexicalEntry, graphs);

    }

    @Override
    public void removeIsEvokedBy(ARTURIResource lexicalConcept, ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalConcept, OntoLex.Res.IS_EVOKED_BY, lexicalEntry, graphs);

    }

    @Override
    public ARTURIResourceIterator listLexicalizedSense(ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalConcept, OntoLex.Res.LEXICALIZED_SENSE, true, graphs));

    }

    @Override
    public void addLexicalizedSense(ARTURIResource lexicalConcept, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalConcept, OntoLex.Res.LEXICALIZED_SENSE, lexicalSense, graphs);

    }

    @Override
    public void removeLexicalizedSense(ARTURIResource lexicalConcept, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalConcept, OntoLex.Res.LEXICALIZED_SENSE, lexicalSense, graphs);

    }

    @Override
    public ARTURIResourceIterator listIsConceptOf(ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalConcept, OntoLex.Res.IS_CONCEPT_OF, true, graphs));

    }

    @Override
    public void addIsConceptOf(ARTURIResource lexicalConcept, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalConcept, OntoLex.Res.IS_CONCEPT_OF, ontologyEntity, graphs);

    }

    @Override
    public void removeIsConceptOf(ARTURIResource lexicalConcept, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalConcept, OntoLex.Res.IS_CONCEPT_OF, ontologyEntity, graphs);

    }

    @Override
    public ARTURIResource getReference(ARTURIResource lexicalSense, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicalSense, OntoLex.Res.REFERENCE, true, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setReference(ARTURIResource lexicalSense, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalSense, OntoLex.Res.REFERENCE, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicalSense, OntoLex.Res.REFERENCE, ontologyEntity, graphs);

    }

    @Override
    public ARTURIResource getIsSenseOf(ARTURIResource lexicalSense, ARTResource... graphs) throws ModelAccessException {

        try (ARTNodeIterator it = baseRep.listValuesOfSubjPredPair(lexicalSense, OntoLex.Res.IS_SENSE_OF, true, graphs)) {

            if (it.hasNext()) {
                ARTNode next = it.next();

                if (next.isURIResource()) {
                    return next.asURIResource();
                }
            }
        }

        return null;

    }

    @Override
    public void setIsSenseOf(ARTURIResource lexicalSense, ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalSense, OntoLex.Res.IS_SENSE_OF, NodeFilters.ANY, graphs);
        baseRep.addTriple(lexicalSense, OntoLex.Res.IS_SENSE_OF, lexicalEntry, graphs);

    }

    @Override
    public ARTURIResourceIterator listIsLexicalizedSenseOf(ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(lexicalConcept, OntoLex.Res.IS_LEXICALIZED_SENSE_OF, true, graphs));

    }

    @Override
    public void addIsLexicalizedSenseOf(ARTURIResource lexicalConcept, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(lexicalConcept, OntoLex.Res.IS_LEXICALIZED_SENSE_OF, ontologyEntity, graphs);

    }

    @Override
    public void removeIsLexicalizedSenseOf(ARTURIResource lexicalConcept, ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(lexicalConcept, OntoLex.Res.IS_LEXICALIZED_SENSE_OF, ontologyEntity, graphs);

    }

    @Override
    public ARTURIResourceIterator listConcepts(ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(ontologyEntity, OntoLex.Res.CONCEPT, true, graphs));

    }

    @Override
    public void addConcept(ARTURIResource ontologyEntity, ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(ontologyEntity, OntoLex.Res.CONCEPT, lexicalConcept, graphs);

    }

    @Override
    public void removeConcept(ARTURIResource ontologyEntity, ARTURIResource lexicalConcept, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(ontologyEntity, OntoLex.Res.CONCEPT, lexicalConcept, graphs);

    }

    @Override
    public ARTURIResourceIterator listIsReferenceOf(ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(ontologyEntity, OntoLex.Res.IS_REFERENCE_OF, true, graphs));

    }

    @Override
    public void addIsReferenceOf(ARTURIResource ontologyEntity, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(ontologyEntity, OntoLex.Res.IS_REFERENCE_OF, lexicalSense, graphs);

    }

    @Override
    public void removeIsReferenceOf(ARTURIResource ontologyEntity, ARTURIResource lexicalSense, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(ontologyEntity, OntoLex.Res.IS_REFERENCE_OF, lexicalSense, graphs);

    }

    @Override
    public ARTURIResourceIterator listIsDenotedBy(ARTURIResource ontologyEntity, ARTResource... graphs) throws ModelAccessException {

        return new URIResourceIteratorFilteringNodeIterator(baseRep.listValuesOfSubjPredPair(ontologyEntity, OntoLex.Res.IS_DENOTED_BY, true, graphs));

    }

    @Override
    public void addIsDenotedBy(ARTURIResource ontologyEntity, ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.addTriple(ontologyEntity, OntoLex.Res.IS_DENOTED_BY, lexicalEntry, graphs);

    }

    @Override
    public void removeIsDenotedBy(ARTURIResource ontologyEntity, ARTURIResource lexicalEntry, ARTResource... graphs) throws ModelUpdateException {

        baseRep.deleteTriple(ontologyEntity, OntoLex.Res.IS_DENOTED_BY, lexicalEntry, graphs);

    }

}
