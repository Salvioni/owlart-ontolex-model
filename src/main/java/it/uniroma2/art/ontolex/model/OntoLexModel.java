
package it.uniroma2.art.ontolex.model;

import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelUpdateException;
import it.uniroma2.art.owlart.model.ARTLiteral;
import it.uniroma2.art.owlart.model.ARTResource;
import it.uniroma2.art.owlart.model.ARTURIResource;
import it.uniroma2.art.owlart.models.RDFModel;
import it.uniroma2.art.owlart.navigation.ARTLiteralIterator;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;

public interface OntoLexModel extends RDFModel{

    //CLASSES
    
    public boolean isAffix(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isConceptSet(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isForm(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isLexicalConcept(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isLexicalEntry(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isLexicalSense(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isMultiwordExpression(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    public boolean isWord(ARTResource resource, ARTResource... graphs) throws ModelAccessException;
    
    public ARTURIResourceIterator listAffixes(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listConceptSet(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listForms(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listFormsByWrittenRep(String term,String language,ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listFormsByWrittenRep(ARTLiteral literal,ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalConcepts(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalEntries(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalEntriesByLexicalForm(ARTURIResource lexicalForm,ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listLexicalSenses(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listMultiwordExpressions(ARTResource... graphs) throws ModelAccessException;
    public ARTURIResourceIterator listWords(ARTResource... graphs) throws ModelAccessException;
    
    
    //LEXICAL ENTRY

    public ARTURIResourceIterator listOtherForms(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addOtherForm(ARTURIResource lexicalEntry,ARTURIResource form,ARTResource... graphs) throws ModelUpdateException ;
    public void removeOtherForm(ARTURIResource lexicalEntry,ARTURIResource form,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResource getCanonicalForm(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void setCanonicalForm(ARTURIResource lexicalEntry,ARTURIResource form,ARTResource... graphs) throws ModelUpdateException;
    public void removeCanonicalForm(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException;
   
    public ARTURIResource getLanguage(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void setLanguage(ARTURIResource lexicalEntry,ARTURIResource language,ARTResource... graphs) throws ModelUpdateException; 
    
    public ARTURIResourceIterator listLexicalForms(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addLexicalForm(ARTURIResource lexicalEntry,ARTURIResource form,ARTResource... graphs) throws ModelUpdateException ;
    public void removeLexicalForm(ARTURIResource lexicalEntry,ARTURIResource form,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listDenotes(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addDenotes(ARTURIResource lexicalEntry,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException ;
    public void removeDenotes(ARTURIResource lexicalEntry,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listSenses(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addSense(ARTURIResource lexicalEntry,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException ;
    public void removeSense(ARTURIResource lexicalEntry,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listEvokes(ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelAccessException;
    public void addEvokes(ARTURIResource lexicalEntry,ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelUpdateException ;
    public void removeEvokes(ARTURIResource lexicalEntry,ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelUpdateException;

    //FORM
    
    public ARTLiteralIterator listWrittenReps(ARTURIResource form,ARTResource... graphs) throws ModelAccessException;
    public ARTLiteralIterator listWrittenReps(ARTURIResource form,String language,ARTResource... graphs) throws ModelAccessException;    
    public void addWrittenRep(ARTURIResource form,ARTLiteral writtenRep,ARTResource... graphs) throws ModelUpdateException ;
    public void addWrittenRep(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
    public void removeWrittenRep(ARTURIResource form,ARTLiteral writtenRep,ARTResource... graphs) throws ModelUpdateException;
    public void removeWrittenRep(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
   
    public ARTLiteralIterator listPhoneticReps(ARTURIResource form,ARTResource... graphs) throws ModelAccessException;
    public ARTLiteralIterator listPhoneticReps(ARTURIResource form,String language,ARTResource... graphs) throws ModelAccessException;    
    public void addPhoneticRep(ARTURIResource form,ARTLiteral phoneticRep,ARTResource... graphs) throws ModelUpdateException ;
    public void addPhoneticRep(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
    public void removePhoneticRep(ARTURIResource form,ARTLiteral phoneticRep,ARTResource... graphs) throws ModelUpdateException;
    public void removePhoneticRep(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
   
    public ARTLiteralIterator listRepresentations(ARTURIResource form,ARTResource... graphs) throws ModelAccessException;
    public ARTLiteralIterator listRepresentations(ARTURIResource form,String language,ARTResource... graphs) throws ModelAccessException;    
    public void addRepresentation(ARTURIResource form,ARTLiteral representation,ARTResource... graphs) throws ModelUpdateException ;
    public void addRepresentation(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
    public void removeRepresentation(ARTURIResource form,ARTLiteral representation,ARTResource... graphs) throws ModelUpdateException;
    public void removeRepresentation(ARTURIResource form,String term,String language,ARTResource... graphs) throws ModelUpdateException ;
   
    //LEXICAL CONCEPT
    
    public ARTURIResourceIterator listIsEvokedBy(ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelAccessException;
    public void addIsEvokedBy(ARTURIResource lexicalConcept,ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException ;
    public void removeIsEvokedBy(ARTURIResource lexicalConcept,ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listLexicalizedSense(ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelAccessException;
    public void addLexicalizedSense(ARTURIResource lexicalConcept,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException ;
    public void removeLexicalizedSense(ARTURIResource lexicalConcept,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException;
    
    public ARTURIResourceIterator listIsConceptOf(ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelAccessException;
    public void addIsConceptOf(ARTURIResource lexicalConcept,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException ;
    public void removeIsConceptOf(ARTURIResource lexicalConcept,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException;

  
    //LEXICAL SENSE
    
    public ARTURIResource getReference(ARTURIResource lexicalSense,ARTResource... graphs) throws ModelAccessException;
    public void setReference(ARTURIResource lexicalSense,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException; 
   
    public ARTURIResource getIsSenseOf(ARTURIResource lexicalSense,ARTResource... graphs) throws ModelAccessException;
    public void setIsSenseOf(ARTURIResource lexicalSense,ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException; 
    
    public ARTURIResourceIterator listIsLexicalizedSenseOf(ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelAccessException;
    public void addIsLexicalizedSenseOf(ARTURIResource lexicalConcept,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException ;
    public void removeIsLexicalizedSenseOf(ARTURIResource lexicalConcept,ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelUpdateException;

    //ONTOLOGY ENTITY
    
    public ARTURIResourceIterator listConcepts(ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelAccessException;
    public void addConcept(ARTURIResource ontologyEntity,ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelUpdateException ;
    public void removeConcept(ARTURIResource ontologyEntity,ARTURIResource lexicalConcept,ARTResource... graphs) throws ModelUpdateException;

    public ARTURIResourceIterator listIsReferenceOf(ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelAccessException;
    public void addIsReferenceOf(ARTURIResource ontologyEntity,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException ;
    public void removeIsReferenceOf(ARTURIResource ontologyEntity,ARTURIResource lexicalSense,ARTResource... graphs) throws ModelUpdateException;

    public ARTURIResourceIterator listIsDenotedBy(ARTURIResource ontologyEntity,ARTResource... graphs) throws ModelAccessException;
    public void addIsDenotedBy(ARTURIResource ontologyEntity,ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException ;
    public void removeIsDenotedBy(ARTURIResource ontologyEntity,ARTURIResource lexicalEntry,ARTResource... graphs) throws ModelUpdateException;


}
