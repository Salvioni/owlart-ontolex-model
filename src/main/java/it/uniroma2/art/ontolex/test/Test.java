package it.uniroma2.art.ontolex.test;

import it.uniroma2.art.ontolex.facilities.OWLArtModelFactoryWithOntoLexModel;
import it.uniroma2.art.ontolex.model.OntoLexModel;
import it.uniroma2.art.owlart.exceptions.ModelAccessException;
import it.uniroma2.art.owlart.exceptions.ModelCreationException;
import it.uniroma2.art.owlart.models.conf.ModelConfiguration;
import it.uniroma2.art.owlart.navigation.ARTURIResourceIterator;
import it.uniroma2.art.owlart.sesame2impl.factory.ARTModelFactorySesame2Impl;


public class Test {

    public static void main(String[] args) throws ModelCreationException, ModelAccessException {

        ARTModelFactorySesame2Impl factImpl = new ARTModelFactorySesame2Impl();

        OWLArtModelFactoryWithOntoLexModel<? extends ModelConfiguration> fact;
        fact = OWLArtModelFactoryWithOntoLexModel.createModelFactory(factImpl);

        OntoLexModel m = fact.loadOntoLexModel("http://localhost:8080/graphdb-workbench-free/repositories/onto");

        m.listFormsByWrittenRep("ciao","en");
        
        try (ARTURIResourceIterator it = m.listLexicalEntries()) {

            while (it.hasNext()) {
                System.out.println(it.next().getLocalName());
            }
        }
        
        

    }

}
